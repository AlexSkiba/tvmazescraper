using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Logic;
using Shouldly;

namespace Rtl.TvMazeScraper.Domain.Tests.ScraperTests
{
    public class Scrape
    {
        private IShowRepository _showRepositoryMock;
        private ITvMazeClient _tvMazeClientMock;
        private IScrapeConfigProvider _configProviderMock;

        [SetUp]
        public void Setup()
        {
            _showRepositoryMock = Substitute.For<IShowRepository>();
            _tvMazeClientMock = Substitute.For<ITvMazeClient>();
            _configProviderMock = Substitute.For<IScrapeConfigProvider>();

            _tvMazeClientMock.ShowsCountPerPage.Returns(250);
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            _configProviderMock.ApiCallTriesCount.Returns(3);
        }

        [Test]
        public async Task Scrape_WhenClientReturnsRateExceededMoreThatTriesCountTimes_ShouldReturnCallRateExceeded()
        {
            _configProviderMock.ApiCallTriesCount.Returns(1);
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.CallRateExceeded));
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Scrape();

            actual.ShouldBe(ScrapeResult.CallRateExceeded);
        }

        [Test]
        public async Task Scrape_WhenReceivedRateExceededFromClient_ShouldRetry()
        {
            _configProviderMock.ApiCallTriesCount.Returns(2);
            var apiResponses = new Queue<TvMazeApiResponse<List<int>>>();
            apiResponses.Enqueue(GetFakeIndexApiResponse(TvMazeApiCallResult.CallRateExceeded));
            apiResponses.Enqueue(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(ci => apiResponses.Dequeue());
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Scrape();

            actual.ShouldBe(ScrapeResult.AllDone);
            await _tvMazeClientMock.Received(2).GetShowsIndex(Arg.Any<int>());
        }

        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(248, 0)]
        [TestCase(249, 1)]
        [TestCase(250, 1)]
        [TestCase(499, 2)]
        [TestCase(500, 2)]
        public async Task Scrape_ShouldCalculateCorrectPageToDownload(int lastShowId, int expectedPage)
        {
            _showRepositoryMock.GetLastId().Returns(lastShowId);
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            await target.Scrape();

            await _tvMazeClientMock.Received(1).GetShowsIndex(Arg.Any<int>());
            await _tvMazeClientMock.Received(1).GetShowsIndex(expectedPage);
        }

        [Test]
        public async Task Scrape_WhenThereIsNoNewShowsToDownload_ShouldReturnAllDone()
        {
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Scrape();

            actual.ShouldBe(ScrapeResult.AllDone);
        }

        [Test]
        public async Task Scrape_WhenAllShowsFromTheFirstPageAreDownloaded_ShouldRequestSecondPage()
        {
            _showRepositoryMock.GetLastId().Returns(2);
            var fakeShowIds = new List<int> {1, 2};
            _tvMazeClientMock.GetShowsIndex(0).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, fakeShowIds));
            _tvMazeClientMock.GetShowsIndex(1).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success));

            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Scrape();

            await _tvMazeClientMock.Received(1).GetShowsIndex(0);
            await _tvMazeClientMock.Received(1).GetShowsIndex(1);
        }

        [Test]
        public async Task Scrape_ShouldSaveNewReceivedShows()
        {
            var savedShows = new List<Show>();
            await _showRepositoryMock.Save(Arg.Do<List<Show>>(s => savedShows.AddRange(s)));
            _showRepositoryMock.GetLastId().Returns(249);
            var fakeShows = new List<Show>
            {
                new Show {Id = 1},
                new Show {Id = 260},
                new Show {Id = 511},
                new Show {Id = 754},
                new Show {Id = 1009}
            };
            _tvMazeClientMock.GetShowsIndex(0).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, new List<int> {fakeShows[0].Id}));
            _tvMazeClientMock.GetShowsIndex(1).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, new List<int> {fakeShows[1].Id}));
            _tvMazeClientMock.GetShowsIndex(2).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, new List<int> {fakeShows[2].Id}));
            _tvMazeClientMock.GetShowsIndex(3).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, new List<int> {fakeShows[3].Id}));
            _tvMazeClientMock.GetShowsIndex(4).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, new List<int> {fakeShows[4].Id}));
            _tvMazeClientMock.GetShowsIndex(Arg.Is<int>(page => page < 0 || page > 4)).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            _tvMazeClientMock.GetShow(fakeShows[0].Id).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[0]));
            _tvMazeClientMock.GetShow(fakeShows[1].Id).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[1]));
            _tvMazeClientMock.GetShow(fakeShows[2].Id).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[2]));
            _tvMazeClientMock.GetShow(fakeShows[3].Id).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[3]));
            _tvMazeClientMock.GetShow(fakeShows[4].Id).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[4]));
            _tvMazeClientMock.GetShow(Arg.Is<int>(id => fakeShows.All(s => s.Id != id))).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.NotFound));
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Scrape();

            savedShows.Count.ShouldBe(4);
            savedShows.ShouldContain(s => s.Id == 260);
            savedShows.ShouldContain(s => s.Id == 511);
            savedShows.ShouldContain(s => s.Id == 754);
            savedShows.ShouldContain(s => s.Id == 1009);
        }

        private static TvMazeApiResponse<List<int>> GetFakeIndexApiResponse(TvMazeApiCallResult callResult, List<int> payload = null)
        {
            return new TvMazeApiResponse<List<int>> {Result = callResult, Payload = payload};
        }

        private static TvMazeApiResponse<Show> GetFakeShowApiResponse(TvMazeApiCallResult callResult, Show payload = null)
        {
            return new TvMazeApiResponse<Show> {Result = callResult, Payload = payload};
        }
    }
}