﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Logic;
using Shouldly;

namespace Rtl.TvMazeScraper.Domain.Tests.ScraperTests
{
    [TestFixture]
    public class Update
    {
        private IShowRepository _showRepositoryMock;
        private ITvMazeClient _tvMazeClientMock;
        private IScrapeConfigProvider _configProviderMock;

        [SetUp]
        public void Setup()
        {
            _showRepositoryMock = Substitute.For<IShowRepository>();
            _tvMazeClientMock = Substitute.For<ITvMazeClient>();
            _configProviderMock = Substitute.For<IScrapeConfigProvider>();

            _configProviderMock.ApiCallTriesCount.Returns(3);
        }

        [Test]
        public async Task ShouldSaveUpdatedShows()
        {
            var updatedTimestamp = DateTimeOffset.Parse("2019-01-01");
            var showUpdate = new ShowUpdate {ShowId = 1, UpdatedTimestamp = updatedTimestamp};
            var updatedShow = new Show {Id = 1, Name = "The Test Show", UpdatedTimestamp = updatedTimestamp};
            _tvMazeClientMock.GetShowsUpdates().Returns(GetFakeShowUpdatesApiRsponse(TvMazeApiCallResult.Success, showUpdate));
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, updatedShow));
            _showRepositoryMock.GetLastUpdatedTimestamp().Returns(updatedTimestamp.AddDays(-1));
            var savedShows = new List<Show>();
            await _showRepositoryMock.Save(Arg.Do<IEnumerable<Show>>(s => savedShows.AddRange(s)));
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            var actual = await target.Update();

            actual.ShouldBe(ScrapeResult.AllDone);
            savedShows.Count.ShouldBe(1);
            savedShows[0].Id.ShouldBe(updatedShow.Id);
            savedShows[0].Name.ShouldBe(updatedShow.Name);
        }

        [Test]
        public async Task WhenShowUpdatedTimestampBeforeSpecified_ShouldNotUpdateThatShow()
        {
            var updatedTimestamp = DateTimeOffset.Parse("2019-01-01");
            var showUpdate = new ShowUpdate {ShowId = 1, UpdatedTimestamp = updatedTimestamp};
            var updatedShow = new Show {Id = 1, Name = "The Test Show", UpdatedTimestamp = updatedTimestamp};
            _tvMazeClientMock.GetShowsUpdates().Returns(GetFakeShowUpdatesApiRsponse(TvMazeApiCallResult.Success, showUpdate));
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, updatedShow));
            _showRepositoryMock.GetLastUpdatedTimestamp().Returns(updatedTimestamp);
            var target = new Scraper(_showRepositoryMock, _tvMazeClientMock, _configProviderMock);

            await target.Update();

            await _tvMazeClientMock.Received(0).GetShow(Arg.Any<int>());
        }

        private static TvMazeApiResponse<Show> GetFakeShowApiResponse(TvMazeApiCallResult callResult, Show payload = null)
        {
            return new TvMazeApiResponse<Show> {Result = callResult, Payload = payload};
        }

        private static TvMazeApiResponse<List<ShowUpdate>> GetFakeShowUpdatesApiRsponse(TvMazeApiCallResult callResult, ShowUpdate payload = null)
        {
            return new TvMazeApiResponse<List<ShowUpdate>> {Result = callResult, Payload = new List<ShowUpdate> {payload}};
        }
    }
}