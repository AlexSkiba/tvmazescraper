﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Exceptions;
using Rtl.TvMazeScraper.Domain.Logic;
using Shouldly;

namespace Rtl.TvMazeScraper.Domain.Tests.DownloaderTests
{
    [TestFixture]
    public class GetShow
    {
        private ITvMazeClient _tvMazeClientMock;

        [SetUp]
        public void Setup()
        {
            _tvMazeClientMock = Substitute.For<ITvMazeClient>();
        }

        [Test]
        public async Task WhenNotFound_ShouldReturnNull()
        {
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.NotFound));
            var target = new Downloader(_tvMazeClientMock, 2);

            var actual = await target.GetShow(1);

            actual.ShouldBeNull();
        }

        [Test]
        public async Task WhenRateExceeded_ShouldRetry()
        {
            var apiResponses = new Queue<TvMazeApiResponse<Show>>();
            apiResponses.Enqueue(GetFakeShowApiResponse(TvMazeApiCallResult.CallRateExceeded));
            apiResponses.Enqueue(GetFakeShowApiResponse(TvMazeApiCallResult.NotFound));
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(ci => apiResponses.Dequeue());
            var target = new Downloader(_tvMazeClientMock, 2);

            await target.GetShow(1);

            await _tvMazeClientMock.Received(2).GetShow(1);
        }

        [Test]
        public async Task WhenSuccess_ShouldReturnShow()
        {
            var fakeShow = new Show {Id = 5, Name = "The Test Show"};
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShow));
            var target = new Downloader(_tvMazeClientMock, 2);

            var actual = await target.GetShow(1);

            actual.Id.ShouldBe(fakeShow.Id);
            actual.Name.ShouldBe(fakeShow.Name);
        }

        [Test]
        public void WhenTooManyRateExceeded_ShouldThrow()
        {
            _tvMazeClientMock.GetShow(Arg.Any<int>()).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.CallRateExceeded));
            var target = new Downloader(_tvMazeClientMock, 2);

            Should.Throw<CallsRateExceededException>(async () => await target.GetShow(1));
        }

        private static TvMazeApiResponse<Show> GetFakeShowApiResponse(TvMazeApiCallResult callResult, Show payload = null)
        {
            return new TvMazeApiResponse<Show> {Result = callResult, Payload = payload};
        }
    }
}