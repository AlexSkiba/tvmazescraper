﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Exceptions;
using Rtl.TvMazeScraper.Domain.Logic;
using Shouldly;

namespace Rtl.TvMazeScraper.Domain.Tests.DownloaderTests
{
    internal class GetShowsFromPage
    {
        private ITvMazeClient _tvMazeClientMock;

        [SetUp]
        public void Setup()
        {
            _tvMazeClientMock = Substitute.For<ITvMazeClient>();

            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
        }

        [Test]
        public async Task WhenRateExceeded_ShouldRetry()
        {
            var apiResponses = new Queue<TvMazeApiResponse<List<int>>>();
            apiResponses.Enqueue(GetFakeIndexApiResponse(TvMazeApiCallResult.CallRateExceeded));
            apiResponses.Enqueue(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(ci => apiResponses.Dequeue());
            var target = new Downloader(_tvMazeClientMock, 2);

            await target.GetShowsFromPage(1);

            await _tvMazeClientMock.Received(2).GetShowsIndex(1);
        }

        [Test]
        public async Task WhenNotFound_ShouldReturnNull()
        {
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.NotFound));
            var target = new Downloader(_tvMazeClientMock, 2);

            var actual = await target.GetShowsFromPage(1);

            actual.ShouldBeNull();
        }

        [Test]
        public async Task WhenSuccess_ShouldReturnShows()
        {
            var fakeShows = new List<Show>
            {
                new Show {Id = 5, Name = "The Test Show 1"},
                new Show {Id = 15, Name = "The Test Show 2"}
            };
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.Success, fakeShows.Select(s => s.Id).ToList()));
            _tvMazeClientMock.GetShow(5).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[0]));
            _tvMazeClientMock.GetShow(15).Returns(GetFakeShowApiResponse(TvMazeApiCallResult.Success, fakeShows[1]));
            var target = new Downloader(_tvMazeClientMock, 2);

            var actual = await target.GetShowsFromPage(1);

            actual.Count.ShouldBe(2);
            actual[0].Id.ShouldBe(fakeShows[0].Id);
            actual[0].Name.ShouldBe(fakeShows[0].Name);
            actual[1].Id.ShouldBe(fakeShows[1].Id);
            actual[1].Name.ShouldBe(fakeShows[1].Name);
        }

        [Test]
        public void WhenTooManyRateExceeded_ShouldThrow()
        {
            _tvMazeClientMock.GetShowsIndex(Arg.Any<int>()).Returns(GetFakeIndexApiResponse(TvMazeApiCallResult.CallRateExceeded));
            var target = new Downloader(_tvMazeClientMock, 2);

            Should.Throw<CallsRateExceededException>(async () => await target.GetShowsFromPage(1));
        }

        private static TvMazeApiResponse<List<int>> GetFakeIndexApiResponse(TvMazeApiCallResult callResult, List<int> payload = null)
        {
            return new TvMazeApiResponse<List<int>> {Result = callResult, Payload = payload};
        }

        private static TvMazeApiResponse<Show> GetFakeShowApiResponse(TvMazeApiCallResult callResult, Show payload = null)
        {
            return new TvMazeApiResponse<Show> {Result = callResult, Payload = payload};
        }
    }
}