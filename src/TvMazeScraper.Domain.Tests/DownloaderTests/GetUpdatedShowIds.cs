﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Exceptions;
using Rtl.TvMazeScraper.Domain.Logic;
using Shouldly;

namespace Rtl.TvMazeScraper.Domain.Tests.DownloaderTests
{
    [TestFixture]
    public class GetUpdatedShowIds
    {
        private ITvMazeClient _tvMazeClientMock;

        [SetUp]
        public void Setup()
        {
            _tvMazeClientMock = Substitute.For<ITvMazeClient>();
        }

        [Test]
        public async Task WhenRateExceeded_ShouldRetry()
        {
            var apiResponses = new Queue<TvMazeApiResponse<List<ShowUpdate>>>();
            apiResponses.Enqueue(GetFakeShowUpdatesResponse(TvMazeApiCallResult.CallRateExceeded));
            apiResponses.Enqueue(GetFakeShowUpdatesResponse(TvMazeApiCallResult.Success, new ShowUpdate()));
            _tvMazeClientMock.GetShowsUpdates().Returns(ci => apiResponses.Dequeue());
            var target = new Downloader(_tvMazeClientMock, 2);

            await target.GetUpdatedShowIds(DateTimeOffset.MinValue);

            await _tvMazeClientMock.Received(2).GetShowsUpdates();
        }

        [Test]
        public async Task WhenSuccess_ShouldReturnShowUpdates()
        {
            var fakeShowUpdate = new ShowUpdate {ShowId = 1, UpdatedTimestamp = DateTimeOffset.UtcNow};
            _tvMazeClientMock.GetShowsUpdates().Returns(GetFakeShowUpdatesResponse(TvMazeApiCallResult.Success, fakeShowUpdate));
            var target = new Downloader(_tvMazeClientMock, 2);

            var actual = await target.GetUpdatedShowIds(DateTimeOffset.MinValue);

            actual.Count.ShouldBe(1);
            actual[0].ShouldBe(fakeShowUpdate.ShowId);
        }

        [Test]
        public void WhenTooManyRateExceeded_ShouldThrow()
        {
            _tvMazeClientMock.GetShowsUpdates().Returns(GetFakeShowUpdatesResponse(TvMazeApiCallResult.CallRateExceeded));
            var target = new Downloader(_tvMazeClientMock, 2);

            Should.Throw<CallsRateExceededException>(async () => await target.GetUpdatedShowIds(DateTimeOffset.Now));
        }

        private static TvMazeApiResponse<List<ShowUpdate>> GetFakeShowUpdatesResponse(TvMazeApiCallResult callResult, ShowUpdate payload = null)
        {
            return new TvMazeApiResponse<List<ShowUpdate>> {Result = callResult, Payload = new List<ShowUpdate> {payload}};
        }
    }
}