﻿using System.Threading.Tasks;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Exceptions;

namespace Rtl.TvMazeScraper.Domain.Logic
{
    public class Scraper
    {
        private readonly IShowRepository _showRepository;
        private readonly ITvMazeClient _client;
        private readonly Downloader _downloader;

        public Scraper(IShowRepository showRepository, ITvMazeClient tvMazeClient, IScrapeConfigProvider configProvider)
        {
            _showRepository = showRepository;
            _client = tvMazeClient;
            _downloader = new Downloader(_client, configProvider.ApiCallTriesCount);
        }

        /// <summary>
        ///     Downloads and saves all shows that are not yet in database.
        /// </summary>
        public async Task<ScrapeResult> Scrape()
        {
            var lastShowId = await _showRepository.GetLastId();
            var firstNotAddedShowId = lastShowId + 1;
            var page = firstNotAddedShowId / _client.ShowsCountPerPage;

            try
            {
                var shows = await _downloader.GetShowsFromPage(page);

                while (shows != null)
                {
                    await _showRepository.Save(shows);
                    page++;
                    shows = await _downloader.GetShowsFromPage(page);
                }

                return ScrapeResult.AllDone;
            }
            catch (CallsRateExceededException)
            {
                // todo: log
                return ScrapeResult.CallRateExceeded;
            }
        }

        /// <summary>
        ///     Checks if there are any shows updates and re-scrapes updated shows.
        /// </summary>
        public async Task<ScrapeResult> Update()
        {
            var lastUpdated = await _showRepository.GetLastUpdatedTimestamp();
            var updatedShowIds = await _downloader.GetUpdatedShowIds(lastUpdated);

            try
            {
                foreach (var updatedShowId in updatedShowIds)
                {
                    var show = await _downloader.GetShow(updatedShowId);
                    if (show == null)
                    {
                        await _showRepository.Delete(updatedShowId);
                    }
                    else
                    {
                        await _showRepository.Save(new[] {show});
                    }
                }

                return ScrapeResult.AllDone;
            }
            catch (CallsRateExceededException)
            {
                // todo: log
                return ScrapeResult.CallRateExceeded;
            }
        }
    }
}