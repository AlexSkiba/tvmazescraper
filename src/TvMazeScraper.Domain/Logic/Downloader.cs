﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Rtl.TvMazeScraper.Domain.Exceptions;

namespace Rtl.TvMazeScraper.Domain.Logic
{
    internal class Downloader
    {
        private readonly int _apiTriesCount;
        private readonly ITvMazeClient _tvMazeClient;

        public Downloader(ITvMazeClient tvMazeClient, int triesCount = 3)
        {
            _tvMazeClient = tvMazeClient;
            _apiTriesCount = triesCount;
        }

        /// <summary>
        ///     Returns null if there are no shows on the specified page and all following pages.
        ///     Returns an empty list if there are no shows only on the specified page.
        /// </summary>
        public async Task<List<Show>> GetShowsFromPage(int pageIndex)
        {
            var showIds = await ExecuteCallWithRetry(async () => await _tvMazeClient.GetShowsIndex(pageIndex));

            if (showIds == null)
            {
                return null;
            }

            var shows = new List<Show>();
            foreach (var showId in showIds)
            {
                var show = await GetShow(showId);

                if (show != null)
                {
                    shows.Add(show);
                }
            }

            return shows;
        }

        /// <summary>
        ///     Returns null if the show with specified id doeesn't exist.
        /// </summary>
        public async Task<Show> GetShow(int showId)
        {
            return await ExecuteCallWithRetry(async () => await _tvMazeClient.GetShow(showId));
        }

        /// <summary>
        ///     Returns list of shows updated after the specified timestamp.
        /// </summary>
        public async Task<List<int>> GetUpdatedShowIds(DateTimeOffset lastUpdatedTimestamp)
        {
            var updates = await ExecuteCallWithRetry(async () => await _tvMazeClient.GetShowsUpdates());

            var showIdsToUpdate = updates.Where(u => u.UpdatedTimestamp > lastUpdatedTimestamp).Select(u => u.ShowId).ToList();

            return showIdsToUpdate;
        }

        private async Task<TPayload> ExecuteCallWithRetry<TPayload>(Func<Task<TvMazeApiResponse<TPayload>>> call)
        {
            var tryIndex = 0;

            while (tryIndex++ < _apiTriesCount)
            {
                var response = await call();

                switch (response.Result)
                {
                    case TvMazeApiCallResult.Success:
                        return response.Payload;
                    case TvMazeApiCallResult.NotFound: // just a not existing item
                        return default(TPayload);
                    case TvMazeApiCallResult.CallRateExceeded: // wait and go for another attempt
                        await Task.Delay(_tvMazeClient.AllowedCallsInterval);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            throw new CallsRateExceededException(_apiTriesCount, _tvMazeClient.AllowedCallsInterval);
        }
    }
}