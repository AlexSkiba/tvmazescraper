﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;

namespace Rtl.TvMazeScraper.Domain.Logic
{
    public class DefaultTvMazeClient : ITvMazeClient
    {
        public int ShowsCountPerPage => 250;

        public TimeSpan AllowedCallsInterval => TimeSpan.FromSeconds(20);

        public Task<TvMazeApiResponse<List<int>>> GetShowsIndex(int page)
        {
            throw new NotImplementedException();
        }

        public Task<TvMazeApiResponse<Show>> GetShow(int id)
        {
            throw new NotImplementedException();
        }

        public Task<TvMazeApiResponse<List<ShowUpdate>>> GetShowsUpdates()
        {
            throw new NotImplementedException();
        }
    }
}