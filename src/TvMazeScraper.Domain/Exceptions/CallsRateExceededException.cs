﻿using System;

namespace Rtl.TvMazeScraper.Domain.Exceptions
{
    internal class CallsRateExceededException : Exception
    {
        public CallsRateExceededException(int triesCount, TimeSpan interval)
        {
            TriesCount = triesCount;
            RateMeasurementInterval = interval;
        }

        public int TriesCount { get; set; }

        public TimeSpan RateMeasurementInterval { get; set; }

        public override string Message => $"API calls rate exceeded on interval {RateMeasurementInterval}. Tries count: {TriesCount}";
    }
}