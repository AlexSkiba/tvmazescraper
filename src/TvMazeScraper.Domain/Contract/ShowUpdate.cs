﻿using System;

namespace Rtl.TvMazeScraper.Domain.Contract
{
    public class ShowUpdate
    {
        public int ShowId { get; set; }
        public DateTimeOffset UpdatedTimestamp { get; set; }
    }
}