﻿using System;
using System.Collections.Generic;

namespace Rtl.TvMazeScraper.Domain.Contract
{
    public class Show
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset UpdatedTimestamp { get; set; }
        public List<Actor> Cast { get; set; }
    }
}