﻿namespace Rtl.TvMazeScraper.Domain.Contract
{
    public enum TvMazeApiCallResult
    {
        Unknown = 0,
        Success,
        NotFound,
        CallRateExceeded
    }
}