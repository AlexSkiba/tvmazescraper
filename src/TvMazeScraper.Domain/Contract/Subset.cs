﻿using System.Collections.Generic;

namespace Rtl.TvMazeScraper.Domain.Contract
{
    public class Subset<T>
    {
        public int TotalItems { get; set; }
        public List<T> Items { get; set; }
    }
}
