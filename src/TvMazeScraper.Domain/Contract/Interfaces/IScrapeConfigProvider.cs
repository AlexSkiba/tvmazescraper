﻿namespace Rtl.TvMazeScraper.Domain.Contract.Interfaces
{
    public interface IScrapeConfigProvider
    {
        int ApiCallTriesCount { get; }
    }
}