﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rtl.TvMazeScraper.Domain.Contract.Interfaces
{
    public interface IShowRepository
    {
        /// <summary>
        ///     Returns a set of shows defined by the query.
        /// </summary>
        Task<Subset<Show>> Get(PaginatedQuery query);

        /// <summary>
        ///     Returns the biggest show id stored in the database.
        /// </summary>
        Task<int> GetLastId();

        /// <summary>
        ///     Returns the most recent updated timestamp of all shows.
        /// </summary>
        Task<DateTimeOffset> GetLastUpdatedTimestamp();

        /// <summary>
        ///     Inserts or updates shows based on their id.
        /// </summary>
        Task Save(IEnumerable<Show> shows);

        /// <summary>
        ///     Deletes the specified show.
        /// </summary>
        Task Delete(int showId);
    }
}