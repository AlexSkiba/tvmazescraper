﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rtl.TvMazeScraper.Domain.Contract.Interfaces
{
    public interface ITvMazeClient
    {
        /// <summary>
        ///     Defines how many shows there are on one index page, see <see cref="GetShowsIndex" />.
        /// </summary>
        int ShowsCountPerPage { get; }

        /// <summary>
        ///     Defines the time interval when TvMaze API measures the calls rate.
        ///     Use this interval to wait before the next call if you got a <see cref="TvMazeApiCallResult.CallRateExceeded" /> response.
        /// </summary>
        TimeSpan AllowedCallsInterval { get; }

        /// <summary>
        ///     Returns existing show ids for the specified page. Each page by design contains <see cref="ShowsCountPerPage" /> ids,
        ///     but there might be fewer actual existing shows, see http://www.tvmaze.com/api#show-index for details.
        /// </summary>
        /// <param name="page">0-based page index.</param>
        Task<TvMazeApiResponse<List<int>>> GetShowsIndex(int page);

        /// <summary>
        ///     Returns a show by its id.
        /// </summary>
        Task<TvMazeApiResponse<Show>> GetShow(int id);

        /// <summary>
        ///     Returns the updated timestamp for all shows, see http://www.tvmaze.com/api#show-updates for details.
        /// </summary>
        Task<TvMazeApiResponse<List<ShowUpdate>>> GetShowsUpdates();
    }
}