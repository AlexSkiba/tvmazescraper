﻿namespace Rtl.TvMazeScraper.Domain.Contract
{
    public class TvMazeApiResponse<TPayload>
    {
        public TvMazeApiCallResult Result { get; set; }
        public TPayload Payload { get; set; }
    }
}