﻿namespace Rtl.TvMazeScraper.Domain.Contract
{
    public enum ScrapeResult
    {
        Unknown = 0,
        AllDone,
        CallRateExceeded
    }
}