﻿namespace Rtl.TvMazeScraper.Domain.Contract
{
    public class PaginatedQuery
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}