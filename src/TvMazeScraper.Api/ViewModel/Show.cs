﻿using System.Collections.Generic;

namespace Rtl.TvMazeScraper.Api.ViewModel
{
    public class Show
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Actor> Cast { get; set; }
    }
}