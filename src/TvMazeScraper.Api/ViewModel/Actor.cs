﻿using System;

namespace Rtl.TvMazeScraper.Api.ViewModel
{
    public class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset Birthday { get; set; }
    }
}