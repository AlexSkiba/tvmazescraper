﻿using System.Collections.Generic;

namespace Rtl.TvMazeScraper.Api.ViewModel
{
    public class Paginated<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public List<T> Items { get; set; }
        public string Credits => "Powered by http://www.tvmaze.com/api";
    }
}