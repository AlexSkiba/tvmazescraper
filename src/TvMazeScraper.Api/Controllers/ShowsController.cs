﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rtl.TvMazeScraper.Api.Mapping;
using Rtl.TvMazeScraper.Api.ViewModel;
using Rtl.TvMazeScraper.Domain.Contract;
using Rtl.TvMazeScraper.Domain.Contract.Interfaces;
using Show = Rtl.TvMazeScraper.Api.ViewModel.Show;

namespace Rtl.TvMazeScraper.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
        private readonly IShowRepository _showRepository;

        public ShowsController(IShowRepository showRepository)
        {
            _showRepository = showRepository;
        }

        public async Task<Paginated<Show>> Get(int pageIndex, int pageSize)
        {
            var showsSet = await _showRepository.Get(new PaginatedQuery
            {
                PageIndex = pageIndex,
                PageSize = pageSize
            });

            return new Paginated<Show>
            {
                PageSize = pageSize,
                PageIndex = pageIndex,
                TotalItems = showsSet.TotalItems,
                Items = showsSet.Items.Select(ShowMapper.ToViewModel).ToList()
            };
        }
    }
}