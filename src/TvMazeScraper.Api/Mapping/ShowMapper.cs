﻿using System.Linq;
using Rtl.TvMazeScraper.Api.ViewModel;

namespace Rtl.TvMazeScraper.Api.Mapping
{
    public static class ShowMapper
    {
        public static Show ToViewModel(Domain.Contract.Show domainModel)
        {
            return new Show
            {
                Id = domainModel.Id,
                Name = domainModel.Name,
                Cast = domainModel.Cast.Select(ToViewModel).ToList()
            };
        }

        private static Actor ToViewModel(Domain.Contract.Actor domainModel)
        {
            return new Actor
            {
                Id = domainModel.Id,
                Name = domainModel.Name,
                Birthday = domainModel.Birthday
            };
        }
    }
}